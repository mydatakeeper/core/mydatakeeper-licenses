# Makefile

CXX:=$(CROSS_COMPILE)g++
EXTRA_CFLAGS:=-std=c++17 -I./include $(shell pkg-config mydatakeeper --cflags)
EXTRA_LDFLAGS:=$(shell pkg-config mydatakeeper --libs) -ljson -lstdc++fs

TARGET=mydatakeeper-licenses
SRC_FILES=mydatakeeper-licenses.c
GEN_FILES=\
	include/fr.mydatakeeper.Packages.h \
	include/fr.mydatakeeper.Package.h
INC_FILES=\
	include/packages.h \
	include/package.h


all: $(TARGET)

$(TARGET): $(SRC_FILES) $(GEN_FILES) $(INC_FILES)
	$(CXX) $< -o $@ $(CFLAGS) $(EXTRA_CFLAGS) $(LDFLAGS) $(EXTRA_LDFLAGS)

include/%.h: include/%.xml
	dbusxx-xml2cpp $< --adaptor=$@
	sed 's|    connect_signal(\(.*\), \(.*\), \(.*\));|}\n    void connect_\2_signal() {\n    \0|g' -i $@
	sed 's| = 0;| { throw ::DBus::Error("org.freedesktop.DBus.Error.NotImplemented", "Method is not implemented"); }|g' -i $@

clean:
	rm -rf $(TARGET) $(GEN_FILES)
