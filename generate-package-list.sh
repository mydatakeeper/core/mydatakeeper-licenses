#!/bin/bash

PACMAN=${1:-pacman}
CROOT=$2
OUT=${3:-${CROOT}/usr/share/mydatakeeper-licenses/package-list.json}

# Create containing directory
mkdir -p "$(dirname "$OUT")"

# Create packages licences list
(
    echo '{'
    # Get a list of all packages installed
    mapfile -t packages < <($PACMAN -Qq)
    for package in "${packages[@]}"; do
        echo -n "  \"${package}\": {"

        # Get the package version
        version=$($PACMAN -Qi "${package}" | sed -n 's/^Version *: \(.*\)$/\1/p')
        echo -n "\"version\":\"${version}\", \"licenses\": ["

        # Get a list of a package licenses
        mapfile -t licenses < <($PACMAN -Qi "${package}" | sed -n 's/^Licenses *: \(.*\)$/\1/p' | sed 's/  /\n/g')
        for license in "${licenses[@]}"; do
            echo -n "\"${license}\""
            if [ "$license" != "${licenses[-1]}" ]; then
                echo -n ', '
            fi
        done

        echo -n "]}"
        if [ "$package" != "${packages[-1]}" ]; then
            echo ','
        else
            echo
        fi
    done
    echo "}"
) > "$OUT"
