#ifndef PACKAGES_H_INCLUDED
#define PACKAGES_H_INCLUDED

#include <mydatakeeper/dbus.h>

#include "fr.mydatakeeper.Packages.h"

#define PACKAGES_NAME "fr.mydatakeeper.Packages"
#define PACKAGES_PATH "/fr/mydatakeeper/Packages"

class PackagesServer
: public fr::mydatakeeper::Packages_adaptor,
  public DBusAdaptor
{
public:
  PackagesServer(DBus::Connection &connection)
  : DBusAdaptor(connection, PACKAGES_PATH)
  {}

    virtual ::DBus::Variant Get(const std::string& interface_name, const std::string& property_name) {
        InterfaceAdaptor *interface = (InterfaceAdaptor *) find_interface(interface_name);
        for (size_t i = 0; interface->introspect()->properties[i].name != nullptr; ++i)
        {
            const auto& prop = interface->introspect()->properties[i];
            if (prop.name != property_name)
                continue;
            if (!prop.read)
                throw DBus::ErrorAuthFailed("Cannot read property");
            DBus::Variant *value = interface->get_property(property_name);
            if (value == nullptr)
                return DBus::Variant();
            return *value;
        }
        throw DBus::ErrorInvalidArgs("Unknown property");
    }

    virtual std::map< std::string, ::DBus::Variant > GetAll(const std::string& interface_name) {
        std::map< std::string, ::DBus::Variant > ret;
        InterfaceAdaptor *interface = (InterfaceAdaptor *) find_interface(interface_name);
        for (size_t i = 0; interface->introspect()->properties[i].name != nullptr; ++i)
        {
            const auto& prop = interface->introspect()->properties[i];
            if (!prop.read)
                continue;
            DBus::Variant *value = interface->get_property(prop.name);
            ret[prop.name] = (value == nullptr) ? DBus::Variant() : *value;
        }
        return ret;
    }

    virtual void Set(const std::string& interface_name, const std::string& property_name, const ::DBus::Variant& value) {
        InterfaceAdaptor *interface = (InterfaceAdaptor *) find_interface(interface_name);
        for (size_t i = 0; interface->introspect()->properties[i].name != nullptr; ++i)
        {
            const auto& prop = interface->introspect()->properties[i];
            if (prop.name != property_name)
                continue;
            if (!prop.write)
                throw DBus::ErrorAuthFailed("Cannot write property");
            DBus::Variant v = value;
            interface->set_property(property_name, v);
            return;
        }
        throw DBus::ErrorInvalidArgs("Unknown property");
    }
};

#endif//PACKAGES_H_INCLUDED
