#ifndef PACKAGE_H_INCLUDED
#define PACKAGE_H_INCLUDED

#include <mydatakeeper/dbus.h>

#include "fr.mydatakeeper.Package.h"

#include <fstream>

class PackageServer
: public fr::mydatakeeper::Package_adaptor,
  public DBusAdaptor
{
public:
  PackageServer(DBus::Connection &connection, const string& path, const string& name)
  : DBusAdaptor(connection, path)
  , _name(name)
  {}

    virtual std::string License(const std::string& license_name) {
        std::map< std::string, bool > licenses = Licenses();
        if (licenses.find(license_name) != licenses.end() && licenses.at(license_name)) {
            std::string license_file = "/usr/share/licenses/common/" + license_name + "/license.txt";
            std::ifstream ifs(license_file.c_str());
            std::string str((std::istreambuf_iterator<char>(ifs)),
                             std::istreambuf_iterator<char>());
            return str;
        }
        throw DBus::ErrorInvalidArgs("Unknown license");
    }

    virtual std::string CustomFile(const std::string& file_name) {
        std::vector< std::string > customFiles = CustomFiles();
        const auto& it = find(customFiles.begin(), customFiles.end(), file_name);
        if (it != customFiles.end()) {
            std::string custom_file = "/usr/share/licenses/" + _name + '/' + file_name;
            std::ifstream ifs(custom_file.c_str());
            std::string str((std::istreambuf_iterator<char>(ifs)),
                             std::istreambuf_iterator<char>());
            return str;
        }
        throw DBus::ErrorInvalidArgs("Unknown custom file");
    }

    virtual ::DBus::Variant Get(const std::string& interface_name, const std::string& property_name) {
        InterfaceAdaptor *interface = (InterfaceAdaptor *) find_interface(interface_name);
        for (size_t i = 0; interface->introspect()->properties[i].name != nullptr; ++i)
        {
            const auto& prop = interface->introspect()->properties[i];
            if (prop.name != property_name)
                continue;
            if (!prop.read)
                throw DBus::ErrorAuthFailed("Cannot read property");
            DBus::Variant *value = interface->get_property(property_name);
            if (value == nullptr)
                return DBus::Variant();
            return *value;
        }
        throw DBus::ErrorInvalidArgs("Unknown property");
    }

    virtual std::map< std::string, ::DBus::Variant > GetAll(const std::string& interface_name) {
        std::map< std::string, ::DBus::Variant > ret;
        InterfaceAdaptor *interface = (InterfaceAdaptor *) find_interface(interface_name);
        for (size_t i = 0; interface->introspect()->properties[i].name != nullptr; ++i)
        {
            const auto& prop = interface->introspect()->properties[i];
            if (!prop.read)
                continue;
            DBus::Variant *value = interface->get_property(prop.name);
            ret[prop.name] = (value == nullptr) ? DBus::Variant() : *value;
        }
        return ret;
    }

    virtual void Set(const std::string& interface_name, const std::string& property_name, const ::DBus::Variant& value) {
        InterfaceAdaptor *interface = (InterfaceAdaptor *) find_interface(interface_name);
        for (size_t i = 0; interface->introspect()->properties[i].name != nullptr; ++i)
        {
            const auto& prop = interface->introspect()->properties[i];
            if (prop.name != property_name)
                continue;
            if (!prop.write)
                throw DBus::ErrorAuthFailed("Cannot write property");
            DBus::Variant v = value;
            interface->set_property(property_name, v);
            return;
        }
        throw DBus::ErrorInvalidArgs("Unknown property");
    }

private:
    std::string _name;
};

#endif//PACKAGE_H_INCLUDED
