#include <getopt.h>
#include <iostream>

#define VERSION "0.0.1"

using namespace std;

string bus = "system";
string package_list = "/usr/share/mydatakeeper-licenses/package-list.json";


void version(char* arg0) {
    cout << arg0 << endl;
    cout << "\tversion: " << VERSION << endl;

    exit(0);
}

void usage(char* arg0, int err) {
    cout << "usage: " << arg0 << " [options ...]" << endl;
    cout << endl;
    cout << "options: " << endl;
    cout << "    --bus           The type of bus used by Dbus interfaces. Either system or session" << endl;
    cout << "    --package-list  A file describing all the packages installed and their licenses" << endl;
    cout << "    -v,--version    Display the current version and exit" << endl;
    cout << "    -h,--help       Display this help message and exit" << endl;

    exit(err);
}

void parse_arguments(int argc, char** argv) {
    clog << "Parsing arguments" << endl;
    while (1) {
        static struct option long_options[] = {
            {"bus",          required_argument, 0,  0 },
            {"package-list", required_argument, 0,  1 },
            {"version",      no_argument,       0,  'v' },
            {"help",         no_argument,       0,  'h' },
            {0,              0,                 0,  0 }
        };
        int c = getopt_long(argc, argv, "l:vh", long_options, NULL);
        if (c == -1)
            break;

        switch (c)
        {
        case 'v':
            version(argv[0]);
            break;
        case 'h':
            usage(argv[0], 0);
            break;
        case 0:
            bus = string(optarg);
            break;
        case 'l':
        case 1:
            package_list = string(optarg);
            break;
        default:
            usage(argv[0], 2);
            break;
        }
    }

    if (optind < argc) {
        while (optind < argc)
            cerr << "Unkown argument '" << argv[optind++] << "'" << endl;
        usage(argv[0], 2);
    }
}

#include <algorithm>
#include <filesystem>
#include <json/json.hh>
#include <packages.h>
#include <package.h>

DBus::BusDispatcher dispatcher;

int main(int argc, char** argv)
{
    parse_arguments(argc, argv);

    // Setup DBus dispatcher
    DBus::default_dispatcher = &dispatcher;

    // Update variables depending on parameters
    DBus::Connection conn = (bus == "system") ?
        DBus::Connection::SystemBus() :
        DBus::Connection::SessionBus();

    if (!filesystem::exists(package_list)) {
        cerr << "File " << package_list << " does not exists" << endl;
        usage(argv[0], 1);
    }

    clog << "Requesting " << PACKAGES_NAME << " name" << endl;
    conn.request_name(PACKAGES_NAME);

    clog << "Registering " << PACKAGES_PATH << " object" << endl;
    PackagesServer packages(conn);

    const JSON::Object& json = parse_file(package_list.c_str());
    map<string, DBus::Struct<string, map<string, bool > > > package_licenses_list;
    list<PackageServer> package_list;
    for (const auto& it : json)
    {
        const string package_name(it.first);
        stringstream escaped_package_name;
        for (auto& it: package_name) {
            if (('A' <= it && it <= 'Z') || ('a' <= it && it <= 'z') || ('0' <= it && it <= '9'))
                escaped_package_name << it;
            else
                escaped_package_name << "_x" << (int)it << '_';
        }
        const string package_path = "/fr/mydatakeeper/Package/" + escaped_package_name.str();

        clog << "Registering " << package_path << " object" << endl;
        package_list.emplace_back(conn, package_path, package_name);
        PackageServer& package = package_list.back();

        const JSON::Object properties = it.second;

        const string version = properties["version"].as_string();
        package.Version = version;

        const JSON::Array licenses(properties["licenses"]);
        map<string, bool> package_licenses;
        for (const auto& it : licenses)
        {
            string license(it);
            string filename = "/usr/share/licenses/common/" + license + "/license.txt";
            package_licenses[license] = filesystem::exists(filename);
        }
        package.Licenses = package_licenses;

        vector<string> package_custom_files;
        const string package_custom_path = "/usr/share/licenses/" + package_name + '/';
        if (filesystem::exists(package_custom_path)) {
            for (const auto& entry : filesystem::directory_iterator(package_custom_path)) {
                std::string path(entry.path());
                package_custom_files.push_back(path.substr(package_custom_path.size()));
            }
        }
        package.CustomFiles = package_custom_files;

        package_licenses_list[package_name] = {version, package_licenses};
    }
    packages.List = package_licenses_list;

    clog << "Starting DBus main loop" << endl;
    dispatcher.enter();

    return 0;
}